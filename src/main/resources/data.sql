INSERT INTO authors(name, surname, nickname) VALUES ('Isaac', 'Asimov', 'Asimov');
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Fondazione', '23963-2', 'La trilogia asimoviana della Fondazione, uscita in volumi separati dal 1951 al 1953', 'Mondatori', '1951-01-01', 9.00, 0);
INSERT INTO book_author(book_id, author_id) VALUES (1, 1);

INSERT INTO users(username, password) VALUES ('admin', '$2a$10$yxZjf/A5ndtHO7zfZ3HsbuG7AZ2RCCMn75f7FoOO1E2x2/BtJFiK6' );

