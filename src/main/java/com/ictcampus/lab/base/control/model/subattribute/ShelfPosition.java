package com.ictcampus.lab.base.control.model.subattribute;

public class ShelfPosition {
    private int id;
    private int tierNumber;
    private int shelfNumber;

    public ShelfPosition(){}

    public ShelfPosition(int tierNumber, int shelfNumber){
        this.id = id;
        this.tierNumber = tierNumber;
        this.shelfNumber = shelfNumber;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
    public int getTierNumber() {
        return tierNumber;
    }

    public void setTierNumber(int tierNumber) {
        this.tierNumber = tierNumber;
    }

    public int getShelfNumber() {
        return shelfNumber;
    }

    public void setShelfNumber(int shelfNumber) {
        this.shelfNumber = shelfNumber;
    }

    @Override
    public String toString() {
        return "ShelfPosition{" +
                "tierNumber=" + tierNumber +
                ", shelfNumber=" + shelfNumber +
                '}';
    }

}


