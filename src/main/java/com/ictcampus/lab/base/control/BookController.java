package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.exception.NotFoundException;
import com.ictcampus.lab.base.control.model.BookRequest;
import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.service.book.BookService;
import com.ictcampus.lab.base.service.book.model.Book;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
@Slf4j
public class BookController {
	private List<BookResponse> list = new ArrayList<>();
	private final BookService bookService;


	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<Book> getBooks() {
		log.info("Restituisco la lista dei libri [{}]");
		return bookService.getBooks();
	}


	@GetMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public Book getBookById(@PathVariable(name = "id") Long id)  throws NotFoundException {
		log.info("Restituisco il libro con id: {}",id);
		return bookService.findBookById(id);
	}

	@GetMapping(value = "/author/{author}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBookByAuthor( @PathVariable(name = "author") String author) throws NotFoundException {
		log.info("Restituisco il libro con autore [{}]",author);
		return bookService.bookByAuthor(author);
	}

	@PostMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public Long createBook( @RequestBody BookRequest bookRequest) {
		log.info("Creazione di un nuovo libro : [{}]", bookRequest);
		return bookService.createBook(bookRequest);
	}

	@PutMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void editBook( @PathVariable(name = "id") Long id, @RequestBody BookRequest bookRequest
	) {
		log.info( "Modifico il libro con ID [{}] e dati[{}]", id, bookRequest );
		bookService.updateById(id, bookRequest);
	}

	@DeleteMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void deleteBook( @PathVariable(name = "id") Long id
	) {
		log.info("Cancello il libro con ID [{}]", id);
		bookService.deleteById(id);
	}
		}


