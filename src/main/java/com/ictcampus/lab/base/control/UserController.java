package com.ictcampus.lab.base.control;


import com.ictcampus.lab.base.control.model.UserRequest;
import com.ictcampus.lab.base.control.model.UserResponse;
import com.ictcampus.lab.base.service.user.UserService;
import com.ictcampus.lab.base.config.jwt.JwtUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@AllArgsConstructor
@Slf4j

public class UserController {
    @Autowired
    private final UserService userService;

    @Autowired
    private final JwtUtil jwtUtil;

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> registerUser(RequestBody UserRequest userRequest){
        log.info("Register new user: {}", userRequest);
        Long userId = userService.registerUser(userRequest);
        String token = jwtUtil.generateToken(userService.loadUserByUsername(userRequest.getUsername()));
        return ResponseEntity.ok(new UserResponse(userId, token));
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> loginUser(@RequestBody UserRequest userRequest){
        log.info("User login attempt: {}", userRequest);
        UserResponse userResponse = userService.loginUser(userRequest);
        String token = jwtUtil.generateToken(userService.loadUserByUsername(userRequest.getUsername()));
        return ResponseEntity.ok(new UserResponse(userResponse.getId(), token));
    }

}
