package com.ictcampus.lab.base.control.model;

import com.ictcampus.lab.base.control.model.subattribute.ShelfPosition;
import java.util.List;

public class BookRequest {
    private Long id;
    private String title;
    private String description;
    private String extendedDescription;
    private Double price;
    private Double discount;
    private ShelfPosition shelfPosition;
    private String urlThumbnailImage;
    private List<String> imgArrayUrls;
    private String author;
    private String ISBN;
    private String publisher;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExtendedDescription() {
        return extendedDescription;
    }

    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public ShelfPosition getShelfPosition() {
        return shelfPosition;
    }

    public void setShelfPosition(ShelfPosition shelfPosition) {
        this.shelfPosition = shelfPosition;
    }

    public String getUrlThumbnailImage() {
        return urlThumbnailImage;
    }

    public void setUrlThumbnailImage(String urlThumbnailImage) {
        this.urlThumbnailImage = urlThumbnailImage;
    }

    public List<String> getImgArrayUrls() {
        return imgArrayUrls;
    }

    public void setImgArrayUrls(List<String> imgArrayUrls) {
        this.imgArrayUrls = imgArrayUrls;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "BookResponse [id=" + id + ", title=" + title +
                ", description= " + description + ", extendedDescription= " +
                extendedDescription + ", price=" + price + ", discount=" +
                discount + ", shelfPosition=" + shelfPosition + ", urlThumbnailImage=" +
                urlThumbnailImage + ", imgArrayUrls=" + imgArrayUrls +", author" + author + ", ISBN=" + ISBN +
                ", publisher=" + publisher + "]";
    }
}
// metodo per eseguire istruzioni
//attributi
//void non resituisce nulla
