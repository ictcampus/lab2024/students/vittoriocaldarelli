package com.ictcampus.lab.base.service.book.model;

import lombok.Data;

@Data
public class Image {
    private Long id;
    private String title;
    private String url;
    private boolean thumbnail;

}
