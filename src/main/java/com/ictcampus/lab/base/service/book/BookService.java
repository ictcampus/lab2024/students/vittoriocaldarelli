package com.ictcampus.lab.base.service.book;

import com.ictcampus.lab.base.control.model.BookRequest;
import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.control.model.subattribute.ShelfPosition;
import com.ictcampus.lab.base.repository.Book.BookRepositoryCustomImpl;
import com.ictcampus.lab.base.repository.Book.entity.BookEntity;
import com.ictcampus.lab.base.service.book.model.Book;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class BookService {

    @Autowired
    private final BookRepositoryCustomImpl bookRepositoryCustomImpl;

    /**
     * Retrieves all books from the database and converts them to a list of Book objects.
     *
     * @return List<Book> - a list of books.
     */
    public List<Book> getBooks() {
        // Convert each BookEntity to a Book and collect them into a list
        return bookRepositoryCustomImpl.findAll()
                .stream()
                .map(this::convertToBook)
                .collect(Collectors.toList());
    }

    /**
     * Retrieves books by title or search term from the database and converts them to a list of Book objects.
     *
     * @param title  The title to search for.
     * @param search The search term to look for.
     * @return List<Book> - a list of books matching the criteria.
     */
    public List<Book> getBooksCustom(String title, String search) {
        // Convert each BookEntity to a Book and collect them into a list
        return bookRepositoryCustomImpl.findByTitleOrSearch(title, search)
                .stream()
                .map(this::convertToBook)
                .collect(Collectors.toList());
    }

    /**
     * Finds a book by its ID.
     *
     * @param id The ID of the book to find.
     * @return Book - the book with the specified ID, or null if not found.
     */
    public Book findBookById(Long id) {
        // Find the book by ID and convert it to a Book object
        return bookRepositoryCustomImpl.findById(id)
                .map(this::convertToBookResponse)
                .orElse(null);
    }

    /**
     * Finds books by the author's name.
     *
     * @param author The author's name.
     * @return List<Book> - a list of books by the specified author.
     */
    public List<Book> findBookByAuthor(String author) {
        // Convert each BookEntity to a Book and collect them into a list
        return bookRepositoryCustomImpl.findByAuthor(author)
                .stream()
                .map(this::convertToBook)
                .collect(Collectors.toList());
    }

//            public BookResponse streamFindBookById(Long id) {
//                Optional<BookResponse> bookResponseOptional = this.list.stream()
//                        .filter(item -> item.getId().equals(id))
//                        .findFirst();
//                return bookResponseOptional.orElse(null);
//            }

    public Long lastId() {
        Long lastIndex = 1L;
        for (BookResponse bookResponse : this.list) {
            if (bookResponse.getId() >= lastIndex) {
                lastIndex = bookResponse.getId() + 1;
            }
        }
        return lastIndex;
    }

    /**
     * Creates a new book in the database.
     *
     * @param bookRequest The data of the book to create.
     * @return Long - the ID of the newly created book.
     */
    public Long createBook(BookRequest bookRequest) {
        // Convert the BookRequest to a BookEntity and save it
        BookEntity bookEntity = convertToBookEntity(bookRequest);
        return bookRepositoryCustomImpl.save(bookEntity).getId();
    }

    public void updateById(Long id, BookRequest newData) {
        for (BookResponse bookResponse : this.list) {
            if (bookResponse.getId().equals(id)) {
                // Update book details with new data
                bookResponse.setTitle(newData.getTitle());
                bookResponse.setDescription(newData.getDescription());
                bookResponse.setExtendedDescription(newData.getExtendedDescription());
                bookResponse.setPrice(newData.getPrice());
                bookResponse.setDiscount(newData.getDiscount());
                bookResponse.setShelfPosition(newData.getShelfPosition());
                bookResponse.setUrlThumbnailImage(newData.getUrlThumbnailImage());
                bookResponse.setImgArrayUrls(newData.getImgArrayUrls());
                bookResponse.setAuthor(newData.getAuthor());
                bookResponse.setISBN(newData.getISBN());
                bookResponse.setPublisher(newData.getPublisher());
                // Add other fields as necessary
            }
        }
    }

    public void deleteById(Long id) {
        this.list.removeIf(item -> item.getId().equals(id));
    }

    private BookResponse convertToBook(BookEntity bookEntity) {
        //Conversion from entity to business model
        BookResponse bookResponse = new BookResponse();
        bookResponse.setId(bookEntity.getId());
        bookResponse.setTitle(bookEntity.getTitle());
        bookResponse.setDescription(bookEntity.getDescription());
        bookResponse.setExtendedDescription(bookEntity.getExtendedDescription());
        bookResponse.setPrice(bookEntity.getPrice());
        bookResponse.setDiscount(bookEntity.getDiscount());
        bookResponse.setShelfPosition(bookEntity.getShelfPosition());
        bookResponse.setUrlThumbnailImage(bookEntity.getUrlThumbnailImage());
        bookResponse.setImgArrayUrls(bookEntity.getImgArrayUrls());
        bookResponse.setAuthor(bookEntity.getAuthor());
        bookResponse.setISBN(bookEntity.getISBN());
        bookResponse.setPublisher(bookEntity.getPublisher());
        return bookResponse;
    }

    private BookEntity convertToBookEntity(BookRequest bookRequest) {
        //Conversion from business model to entity
        BookEntity bookEntity = new BookEntity();
        bookEntity.setId(bookRequest.getId());
        bookEntity.setTitle(bookRequest.getTitle());
        bookEntity.setDescription(bookRequest.getDescription()); // Added missing fields in `BookResponse` constructor
        bookEntity.setExtendedDescription(bookRequest.getExtendedDescription()); // Added missing fields in `BookResponse` constructor
        bookEntity.setPrice(bookRequest.getPrice()); // Added missing fields in `BookResponse` constructor
        bookEntity.setDiscount(bookRequest.getDiscount()); // Added missing fields in `BookResponse` constructor
        bookEntity.setShelfPosition(bookRequest.getShelfPosition().getId()); // Added missing fields in `BookResponse` constructor
        bookEntity.setUrlThumbnailImage(bookRequest.getUrlThumbnailImage()); // Added missing fields in `BookResponse` constructor
        bookEntity.setImgArrayUrls(String.join(bookRequest.getImgArrayUrls())); // Added missing fields in `BookResponse` constructor
        bookEntity.setAuthor(bookRequest.getAuthor());
        bookEntity.setISBN(bookRequest.getISBN());
        bookEntity.setPublisher(bookRequest.getPublisher());
        return bookResponse;
    }
}

