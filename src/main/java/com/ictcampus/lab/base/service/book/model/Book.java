package com.ictcampus.lab.base.service.book.model;

import com.ictcampus.lab.base.control.model.subattribute.ShelfPosition;
import lombok.Data;
import java.util.List;

@Data

public class Book {
    private Long id;
    private String title;
    private String description;
    private String extendedDescription;
    private Double price;
    private Double discount;
    private ShelfPosition shelfPosition;
    private String urlThumbnailImage;
    private List<String> imgArrayUrls;
    private String author;
    private String ISBN;
    private String publisher;
}
