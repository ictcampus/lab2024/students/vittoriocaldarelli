package com.ictcampus.lab.base.service.book.mapper;

public interface BookServiceStructMapper {

    Long mapToId(BookRequest bookRequest);
}
