package com.ictcampus.lab.base.service.user;

import com.ictcampus.lab.base.entity.UserEntity;
import com.ictcampus.lab.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String username) {
        UserEntity userEntity = userRepository.findByUsername(username);
        if (userEntity == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User( userEntity.getUsername(), userEntity.getPassword(), new ArrayList<>() );
    }
}















//public class UserService {
//    private final UserRepository userRepository;
//
//    public UserService(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }
//
//    public Long registerUser(UserRequest userRequest){
//        UserEntity userEntity = convertToUserEntity(userRequest);
//        userRepository.save(userEntity);
//        return userEntity.getId();
//    }
//    public UserResponse loginUser(UserRequest userRequest){
//        UserEntity userEntity = userRepository.findByUsernameAndPassword(
//        userRequest.getUsername(), userRequest.getPassword());
//        if(userEntity == null){
//            return null; //or throw an appropriate exception
//        }
//        return convertToUserResponse(userEntity);
//    }
//
//    private UserEntity convertToUserEntity(UserRequest userRequest){
//        return new UserEntity(
//                userRequest.getId(),
//                userRequest.getUsername(),
//                userRequest.getPassword(),
//                userRequest.getFirstName(),
//                userRequest.getLastName(),
//                userRequest.getEmail(),
//                userRequest.getShippingAddress(),
//                userRequest.getBillingAddress()
//
//        );
//    }
//
//    private UserResponse convertToUserResponse(UserEntity userEntity){
//        return new UserResponse(
//                userEntity.getId(),
//                userEntity.getUsername(),
//                userEntity.getEmail(),
//                userEntity.getFirstName(),
//                userEntity.getLastName(),
//                userEntity.getShippingAddress(),
//                userEntity.getBillingAddress()
//        );
//    }
/*The convertToUserEntity() method takes the incoming UserRequest object, extracts the attributes,
and uses them to create a new UserEntity object.
This UserEntity object can then be used by the repository layer to interact with the database.
The convertToUserResponse() method takes an incoming UserEntity object
(which represents a user retrieved from the database),
extracts the attributes, and uses them to create a new UserResponse object.
This UserResponse object
 is then returned to the caller, which can be used to
 display the user data in the response sent back to the client.
 * */


}
