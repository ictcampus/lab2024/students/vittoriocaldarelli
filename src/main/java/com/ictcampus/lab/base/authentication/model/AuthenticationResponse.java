package com.ictcampus.lab.base.authentication.model;

public class AuthenticationResponse {

    private String jwt;
}
/* encapsulate the JWT token and any other relevant information.
In this case,
* the AuthenticationResponse class has a single property jwt,
which represents
* the JSON Web Token (JWT) that will be sent back to the client.*/