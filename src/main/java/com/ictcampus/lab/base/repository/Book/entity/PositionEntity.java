package com.ictcampus.lab.base.repository.Book.entity;



import jakarta.persistence.*;
import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@Data
@EntityScan
@Table(name = "positions", uniqueConstraints = @UniqueConstraint(columnNames = {"floor","sector","rack","line"}))
public class PositionEntity {
    @Id //marks the primary key field
    @GeneratedValue(strategy = GenerationType.IDENTITY) //specificies the primary key generation strategy
    private Long id;

    @Column(nullable = false) //maps the fields to database columns
    private String floor;
    @Column(nullable = false)
    private int sector;
    @Column(nullable = false)
    private int rack;
    @Column(nullable = false)
    private int line;
}
