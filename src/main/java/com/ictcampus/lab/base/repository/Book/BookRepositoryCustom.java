package com.ictcampus.lab.base.repository.Book;


import org.springframework.data.jpa.repository.Query;
import com.ictcampus.lab.base.repository.Book.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface BookRepositoryCustom extends JpaRepository<BookEntity, Long> {
    @Query("SELECT b FROM BookEntity b")
    List<BookEntity> findAll();

    @Query("SELECT b FROM BookEntity b WHERE b.id = ?1")
    BookEntity findById(long id);

    @Query("SELECT b FROM BookEntity b WHERE b.title = ?1")
    List<BookEntity> findByTitle(String title);

    //Custom query to find the lastID
    @Query("SELECT b FROM BookEntity b WHERE b.title LIKE %?1% OR b.author LIKE %?2%")
    List<BookEntity> findByTitleOrSearch(String title, String search);

    @Query("SELECT b FROM BookEntity b WHERE b.author LIKE %?1%")
    List<BookEntity> findByAuthor(String author);

}

//The BookRepositoryCustom interface is typically used to define custom methods that are not provided by the JpaRepository interface.
//By implementing the BookRepositoryCustom interface in the BookRepositoryCustomImpl class, you can add custom functionality specific to your application.
//So, the connection between the repository layer and the service layer is established through the use of interfaces.
//The BookRepository interface defines the contract for interacting with the BookEntity objects,
//while the BookRepositoryCustomImpl class implements the custom methods defined in the BookRepositoryCustom interface.