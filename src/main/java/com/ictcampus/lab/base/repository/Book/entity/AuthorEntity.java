package com.ictcampus.lab.base.repository.Book.entity;

import lombok.Data;

import java.time.LocalDate;

@Data
public class AuthorEntity {

    private Long id;
    private String name;
    private String surname;
    private String nickname;
    private LocalDate birthday;



}
