package com.ictcampus.lab.base.repository.User;


//This is a method signature in Java. It defines a method named findByUsernameAndPassword that takes two parameters:
// username of type String and password of type String.
// The method is expected to return an object of type UserEntity.
// The purpose of this method is to find a user in a repository based on their username and password.

import com.ictcampus.lab.base.repository.User.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long>{
    UserEntity findByUsernameAndPassword(String username, String password);


}


