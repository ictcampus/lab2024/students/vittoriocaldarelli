package com.ictcampus.lab.base.repository.Book;

import com.ictcampus.lab.base.repository.Book.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long>, BookRepositoryCustom {
    BookEntity findByTitle(String title);
}
