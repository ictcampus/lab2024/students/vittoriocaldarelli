package com.ictcampus.lab.base.repository.Book;

import com.ictcampus.lab.base.repository.Book.entity.BookEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import lombok.AllArgsConstructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


/*
*
*  In a Spring application, a repository is typically responsible for providing methods to perform CRUD
* (Create, Read, Update, Delete) operations on a certain entity, in this case, WorldEntity.
* The WorldRepository would define methods for interacting with the database where WorldEntity objects are stored.
* These methods could include standard operations like findAll(), findById(), save(), delete(), etc.,
* which are provided by Spring Data JPA repositories.
* It could also include custom methods defined specifically for WorldEntity, such as
* findByNameOrSearch(String name, String search) as seen in your WorldService class
* */
@AllArgsConstructor
@Repository

public abstract class  BookRepositoryCustomImpl implements BookRepositoryCustom {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<BookEntity> findAll() {
    try {
        //Query to find all books in the db
        return jdbcTemplate.query("SELECT * FROM book", new BeanPropertyRowMapper<>(BookEntity.class));
    } catch (DataAccessException e) {
        e.printStackTrace();
        return null;
    }
    }
    @Override
    public BookEntity findById(long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM book WHERE id = ?",
                new BeanPropertyRowMapper<>(BookEntity.class), id);
    }

    public BookEntity findByTitle(String title) {
        // Query to find a book by its title
        return jdbcTemplate.queryForObject("SELECT * FROM book WHERE title = ?",
                new BeanPropertyRowMapper<>(BookEntity.class), title);
    }
    /*
    * This code snippet defines a method named findByTitle that takes a String parameter called title.
    * It uses a JDBC template (jdbcTemplate) to execute a
    * SQL query that selects all columns from a table named book where the name column matches the provided title.
    * The result of the query is then mapped to a BookEntity object using a BeanPropertyRowMapper.
    * The method returns the resulting BookEntity object.*/

    public List<BookEntity> findByAuthor(String author) {
        // Query to find books by author
        return jdbcTemplate.query("SELECT * FROM book WHERE author = ?",
                new BeanPropertyRowMapper<>(BookEntity.class), author);
    }

    public List<BookEntity> findByTitleOrSearch(String title, String search) {
        //Determine filter based on title or search
        String filter = title;
        if (filter == null || filter.isEmpty()) {
            filter = search;
        }
        //add wildcard characters for partial matching
        filter = "%" + filter + "%";

        //Query to find books where the title or search field matches the filter
        return jdbcTemplate.query("SELECT * FROM book WHERE title ILIKE ? OR search ILIKE ?",
                new BeanPropertyRowMapper<>(BookEntity.class), filter, filter);
    }


    public Long create(BookEntity bookEntity) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        //insert a new book into the database
        jdbcTemplate.update( connection -> {
                    PreparedStatement ps = connection.prepareStatement(
                        "INSERT INTO book (title, author," +
                                "description, extendedDescription, price," +
                                " discount, position_id, thumbnail_id," +
                                "isbn, imgURL, publisher ) VALUES "+
                                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, bookEntity.getTitle());
                    ps.setString(2, bookEntity.getAuthor());
                    ps.setString(3, bookEntity.getDescription());
                    ps.setString(4, bookEntity.getExtendedDescription());
                    ps.setDouble(5, bookEntity.getPrice());
                    ps.setDouble(6, bookEntity.getDiscount());
                    ps.setString(7, bookEntity.getShelfPosition());
                    ps.setString(8, bookEntity.getUrlThumbnailImage());
                    ps.setString(9, bookEntity.getISBN());
                    ps.setString(10, bookEntity.getImgArrayUrls());
                    ps.setString(11, bookEntity.getPublisher());
                    return ps;
        }, keyHolder);
        //return the generated id of the new book
        return keyHolder.getKey().longValue();
    }
        public int update ( BookEntity bookEntity){
        return (int) jdbcTemplate.update("UPDATE book SET title = ?, author = ?,"+
                " description = ?, extendedDescription = ?, price = ?, discount = ?,"+
                " position_id = ?, thumbnail_id = ?, isbn = ?, imgURL = ?, publisher = ? "+
                "WHERE id = ?",
                bookEntity.getTitle(),
                bookEntity.getAuthor(),
                bookEntity.getDescription(),
                bookEntity.getExtendedDescription(),
                bookEntity.getPrice(),
                bookEntity.getDiscount(),
                bookEntity.getShelfPosition(),
                bookEntity.getUrlThumbnailImage(),
                bookEntity.getImgArrayUrls(),
                bookEntity.getISBN(),
                bookEntity.getPublisher(),
                bookEntity.getId()
        );
    }
}

